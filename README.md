# Calibrating the Complexity of Propositional Connectives

This code calculates complexity of propositional connectives as described in the paper [Calibrating the Complexity of Ternary Propositional Connectives](https://scholar.rose-hulman.edu/cgi/viewcontent.cgi?article=1131&context=rhumj). The main question is, for an n-ary connective, how many (n-1)-ary connectives are required to form a logically equivalent expression? For example, can all ternary connectives be represented by a formula consisting of at most four binary connectives?

This code uses a brute force approach and incorporates no lemmas (though that logic could be added in to make things run faster). This code can calculate the optimality (see paper) for ternary and quaternary connectives.

Calculating the optimality for ternary connectives will run in minutes. Calculating the optimality for quaternary connectives can take days. The quaternary calculation can run multi-threaded with a specific number of threads. The threading logic is simple, a subtree of the search space to each thread. This code computes a couple million truth tables per second per dedicated thread.

## Building and Running
To build:
```
g++ -std=c++11 .\connectives.cpp -o connectives -pthread
```

To run, for example:
```
.\connectives ternary -t 1
```
or
```
.\connectives quaternary -t 16
```

## Results
The results I got from running the code are as follows:

### Ternary Connectives
| n-optimal | counts |
|-----------|--------|
| 0-optimal | 3      |
| 1-optimal | 35     |
| 2-optimal | 114    |
| 3-optimal | 80     |
| 4-optimal | 24     |

### Quaternary Connectives
| n-optimal | counts |
|-----------|--------|
| 0-optimal | 4      |
| 1-optimal | 874    |
| 2-optimal | 34314  |
| 3-optimal | 30344  |

This quaternary results answer an open question from the paper, showing that all quaternary connectives can be represented by at most three ternary connectives.
