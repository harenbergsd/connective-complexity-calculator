#include <vector>
#include <stdint.h>

using uint = uint16_t;  // quaternary requires 16-bit integer

void usage(const char* more);
void parse_args(int & nary, int & nthreads, int argc, char** argv);
void populate_quaternary_start_states(std::vector<std::vector<uint>> & start_states);

class ConnectiveCalculator {
public:
    int num_tables_evaled;
    std::vector<int> optimality;    // optimality[i] will store the minimum number of (n-1)-ary needed to replicate the i-th n-ary connective

    ConnectiveCalculator(int nary, int max_connectives);
    void run(std::vector<uint> start_connectives);

protected:
    int nary;
    int max_connectives;            // hardcoded depth to stop searching at
    int max_statements;
    int max_formula_size;
    std::vector<uint> stack;
    std::vector<uint> statements;
    std::vector<uint> connectives;

    void build_formula(std::vector<uint>& formula, std::vector<bool>& is_connective, int n_statements, int n_connectives);
    void eval_formula(std::vector<uint>& formula, std::vector<bool>& is_connective);

    virtual bool can_add_connective(int n_connectives, int n_statements) = 0;
    virtual uint apply_connective(uint i, int & idx) = 0;
};


class QuaternaryCalculator : public ConnectiveCalculator {
    public:
        QuaternaryCalculator();

    private:
        uint ternary_connective(uint i, uint P, uint Q, uint R);
        virtual bool can_add_connective(int n_connectives, int n_statements);
        virtual uint apply_connective(uint i, int & idx);
};


class TernaryCalculator : public ConnectiveCalculator {
    public:
        TernaryCalculator();

    private:
        uint binary_connective(uint i, uint P, uint Q);
        virtual bool can_add_connective(int n_connectives, int n_statements);
        virtual uint apply_connective(uint i, int & idx);
};