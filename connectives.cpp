#include <iostream>
#include <thread>
#include <numeric>
#include <algorithm>
#include "connectives.hpp"

#define NUM_BINARY_CONNECTIVES     16       // 2^(2^2)
#define NUM_TERNARY_CONNECTIVES    256      // 2^(2^3)
#define NUM_QUATERNARY_CONNECTIVES 65536    // 2^(2^4)

using ConnectiveCalculatorVec = std::vector<std::shared_ptr<ConnectiveCalculator>>;


int main(int argc, char** argv) {
    int nary;
    int nthreads;
    parse_args(nary, nthreads, argc, argv);
    int unique_connectives{nary == 3 ? NUM_TERNARY_CONNECTIVES : NUM_QUATERNARY_CONNECTIVES};

    ConnectiveCalculatorVec         calcs       (nthreads);
    std::vector<std::thread>        threads     (nthreads);
    std::vector<std::vector<uint>>  start_states(nthreads);

    // seed the starting states of search space
    // each thread gets a unique branch of the search tree
    if (nary == 4) {
        populate_quaternary_start_states(start_states);
        for (int i{0}; i<calcs.size(); i++) {
            calcs[i] = std::make_shared<QuaternaryCalculator>();
        }
    }
    else if (nary == 3) {
        for (int i{0}; i<calcs.size(); i++) {
            calcs[i] = std::make_shared<TernaryCalculator>();
        }
    }
    else {
        usage("Invalid option\n");
    }

    // run the threads
    std::chrono::steady_clock::time_point start{std::chrono::steady_clock::now()};

    for (int i{0}; i<nthreads; i++) {
        std::thread th(&ConnectiveCalculator::run, calcs[i], start_states[i]);
        threads[i] = std::move(th);
    }

    // wait for threads to finish
    for (std::thread & thread : threads) {
        thread.join();
    }

    // gather results from threads and print
    std::vector<int> final_counts(unique_connectives, std::numeric_limits<int>::max());
    uint64_t total_formulas{0};
    for (auto calc : calcs) {
        total_formulas += calc->num_tables_evaled;
        for (int j{0}; j<calc->optimality.size(); j++) {
            if (final_counts[j] > calc->optimality[j]) {
                final_counts[j] = calc->optimality[j];
            }
        }
    }

    std::chrono::steady_clock::time_point end{std::chrono::steady_clock::now()};
    long elapsed{std::chrono::duration_cast<std::chrono::seconds>(end - start).count()};
    std::cout << "elapsed time   = " << elapsed << "s" << std::endl;
    std::cout << "total formulas = " << total_formulas << std::endl;


    int max{*std::max_element(final_counts.begin(), final_counts.end())};
    std::cout << "n-optimal counts:" << std::endl;
    for (int i{0}; i<max+1; i++) {
        int count{0};
        for (int j{0}; j<unique_connectives; j++) {
            if(final_counts[j] == i) {
                count ++;
            }
        }
        std::cout << "  " << i << "-optimal: " << count << std::endl;
    }
}


ConnectiveCalculator::ConnectiveCalculator(int nary, int max_connectives)
    : nary{nary}
    , max_connectives{max_connectives}
    , num_tables_evaled{0}
{}


void ConnectiveCalculator::run(std::vector<uint> formula) {
    int n_statements{static_cast<int>(formula.size())};
    std::vector<bool> is_connective(n_statements, false);
    formula.resize(this->max_formula_size);
    is_connective.resize(this->max_formula_size);
    build_formula(formula, is_connective, n_statements, 0);
}


void ConnectiveCalculator::build_formula(std::vector<uint>& formula, std::vector<bool>& is_connective, int n_statements, int n_connectives) {
    int len{n_statements + n_connectives};

    if (n_statements == this->max_statements && n_connectives == this->max_connectives) {
        this->num_tables_evaled ++;
        eval_formula(formula, is_connective);
        if (this->num_tables_evaled % 100000000 == 0){
            std::cout << "object " << this << " evaluated " << this->num_tables_evaled << " truth tables" << std::endl;
        }
    }
    if (n_statements < this->max_statements){
        for (uint S : statements){
            formula[len] = S;
            is_connective[len] = false;
            build_formula(formula, is_connective, n_statements + 1, n_connectives);
        }
    }
    if (can_add_connective(n_statements, n_connectives)) {
        for (uint connective : this->connectives){
            formula[len] = connective;
            is_connective[len] = true;
            build_formula(formula, is_connective, n_statements, n_connectives + 1);
        }
    }
}


void ConnectiveCalculator::eval_formula(std::vector<uint>& formula, std::vector<bool>& is_connective) {
    int idx{0};
    int n_connectives{0};
    for (int i{0}; i<this->max_formula_size; i++) {
        if(!is_connective[i]) {
            stack[idx ++] = formula[i];
        }
        else {
            n_connectives ++;
            uint res{apply_connective(formula[i], idx)};
            if (this->optimality[res] > n_connectives){
                this->optimality[res] = n_connectives;
            }
            stack[idx-1] = res;
        }
    }
}


TernaryCalculator::TernaryCalculator()
    : ConnectiveCalculator(3, 4)
{
    this->max_statements = max_connectives + 1;
    this->max_formula_size = max_connectives + max_statements;
    this->stack = std::vector<uint>(max_formula_size);
    this->optimality = std::vector<int>(NUM_TERNARY_CONNECTIVES, std::numeric_limits<int>::max());

    uint P{0b11110000};
    uint Q{0b11001100};
    uint R{0b10101010};
    this->statements = {P, Q, R};

    this->connectives.resize(NUM_BINARY_CONNECTIVES);
    std::iota(std::begin(this->connectives), std::end(this->connectives), 0);

    for (uint S : statements) {
        this->optimality[S] = 0;  // statement letters require no connectives
    }
}


bool TernaryCalculator::can_add_connective(int n_statements, int n_connectives) {
    return n_connectives < this->max_connectives && n_connectives + 2 <= n_statements;
}

uint TernaryCalculator::apply_connective(uint i, int & idx) {
    /*
    P | Q | P [i] Q
    1   1      .
    1   0      .
    0   1      .
    0   0      .
    */
    uint8_t res {0};
    uint8_t P   {static_cast<uint8_t>(this->stack[idx-1])};
    uint8_t Q   {static_cast<uint8_t>(this->stack[idx-2])};
    uint8_t nP  {static_cast<uint8_t>(~P)};
    uint8_t nQ  {static_cast<uint8_t>(~Q)};
    if (i & 0b1000) res |= P & Q;
    if (i & 0b0100) res |= P & nQ;
    if (i & 0b0010) res |= nP & Q;
    if (i & 0b0001) res |= nP & nQ;

    idx --;
    return res;
}


QuaternaryCalculator::QuaternaryCalculator()
    : ConnectiveCalculator(4, 3)
{
    this->max_statements = max_connectives * 2 + 1;
    this->max_formula_size = max_connectives + max_statements;
    this->stack = std::vector<uint>(max_formula_size);
    this->optimality = std::vector<int>(NUM_QUATERNARY_CONNECTIVES, std::numeric_limits<int>::max());

    uint P{0b1111111100000000};
    uint Q{0b1111000011110000};
    uint R{0b1100110011001100};
    uint S{0b1010101010101010};
    this->statements = {P, Q, R, S};

    this->connectives.resize(NUM_TERNARY_CONNECTIVES);
    std::iota(std::begin(this->connectives), std::end(this->connectives), 0);

    for (uint S : statements) {
        this->optimality[S] = 0;  // statement letters require no connectives
    }
}


bool QuaternaryCalculator::can_add_connective(int n_statements, int n_connectives) {
    return n_connectives < this->max_connectives && (n_connectives + 1) * 2 + 1 <= n_statements;
}


uint QuaternaryCalculator::apply_connective(uint i, int & idx) {
    /*
    P | Q | R | [i](P,Q,R)
    1   1   1    .
    1   1   0    .
    1   0   1    .
    1   0   0    .
    0   1   1    .
    0   1   0    .
    0   0   1    .
    0   0   0    .
    */
    uint16_t res {0};
    uint16_t P   {static_cast<uint16_t>(this->stack[idx-1])};
    uint16_t Q   {static_cast<uint16_t>(this->stack[idx-2])};
    uint16_t R   {static_cast<uint16_t>(this->stack[idx-3])};
    uint16_t nP  {static_cast<uint16_t>(~P)};
    uint16_t nQ  {static_cast<uint16_t>(~Q)};
    uint16_t nR  {static_cast<uint16_t>(~R)};
    if (i & 0b10000000) res |= P & Q & R;
    if (i & 0b01000000) res |= P & Q & nR;
    if (i & 0b00100000) res |= P & nQ & R;
    if (i & 0b00010000) res |= P & nQ & nR;
    if (i & 0b00001000) res |= nP & Q & R;
    if (i & 0b00000100) res |= nP & Q & nR;
    if (i & 0b00000010) res |= nP & nQ & R;
    if (i & 0b00010001) res |= nP & nQ & nR;

    idx -= 2;
    return res;
}


void populate_quaternary_start_states(std::vector<std::vector<uint>> & start_states) {
    int size = start_states.size();

    uint P{0b1111111100000000};
    uint Q{0b1111000011110000};
    uint R{0b1100110011001100};
    uint S{0b1010101010101010};
    uint statements[4] = {P, Q, R, S};

    if (size == 4)
        for (int i{0}; i<size; i++)
            start_states[i].push_back(statements[i%4]);
    if (size == 16)
        for (int i{0}; i<size; i++)
            start_states[i].push_back(statements[i%4]);
    if (size == 64)
        for (int i{0}; i<size; i++)
            start_states[i].push_back(statements[i%4]);
}


void usage(const char* more) {
    std::cerr << more;
    std::cerr << "usage: ./connectives <ternary | quaternary>" << std::endl;
    std::cerr << "-t number of threads (default is 1)" << std::endl;
    exit(0);
}


void parse_args(int & nary, int & nthreads, int argc, char** argv)
{
    nary = 0;
    nthreads = 1;
    if (argc < 1)
        usage("Incorrect number of arguments\n");

    for (int i{1}; i<argc; i++) {
        if(argv[i][0] == '-') {
            switch(argv[i][1]) {
                case 't':
                    nthreads = atoi(argv[++i]);
                    break;
                default:
                    usage("Invalid option\n");
            }
        }
        else {
            std::string str(argv[i]);
            if (str.compare("ternary") == 0)
                nary = 3;
            else if (str.compare("quaternary") == 0)
                nary = 4;
            else
                usage("Invalid command\n");
        }
    }

    if (nary == 3){
        if (nthreads != 1)
            usage("Invalid command, number of threads must be 1 for the ternary option\n");
    }
    else if (nary == 4){
        if (nthreads != 1 && nthreads != 4 && nthreads != 16 && nthreads != 64)
            usage("Invalid command, number of threads must be 4, 16, or 64 for the quaternary option\n");
    }
    else{
        usage("Invalid command\n");
    }
}